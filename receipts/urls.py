from django.contrib import admin
from django.urls import path
from django.contrib.auth import logout
from .views import ReceiptList, CreateReceipt

urlpatterns = [
    path("", ReceiptList.as_view(), name="home"),
    path("create/", CreateReceipt.as_view(), name="create_receipt"),
]
