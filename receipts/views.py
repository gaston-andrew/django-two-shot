from django.shortcuts import render, redirect
from django.views.generic import ListView
from .models import Receipt
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView


# Create your views here.


class ReceiptList(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/listview.html"
    context_object_name = "receiptlist"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class CreateReceipt(CreateView):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    template_name = "receipts/create.html"
    context_object_name = "create_receipt"

    def form_valid(self, form):
        new_receipt = form.save(commit=False)
        new_receipt.purchaser = self.request.user
        new_receipt.save()

        return redirect("home")
